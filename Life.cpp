//
//  Life.cpp
//
#include <iostream>
#include <vector>
#include "Life.h"
#include "Cell.h"

using namespace std;

int countAliveNeighbours(Board board, short x, short y, short xSize, short ySize) {
    short xminus1 = (x == 0 ? xSize - 1 : x - 1);
    short yminus1 = (y == 0 ? ySize - 1 : y - 1);
    short xplus1  = (x == (xSize - 1) ? 0 : x + 1);
    short yplus1  = (y == (ySize - 1) ? 0 : y + 1);
    short aliveNeighbours = 0;
    aliveNeighbours += board.cellStatus(xminus1, yminus1);
    aliveNeighbours += board.cellStatus(x, yminus1);
    aliveNeighbours += board.cellStatus(xplus1, yminus1);
    aliveNeighbours += board.cellStatus(xminus1, y);
    aliveNeighbours += board.cellStatus(xplus1, y);
    aliveNeighbours += board.cellStatus(xminus1, yplus1);
    aliveNeighbours += board.cellStatus(x, yplus1);
    aliveNeighbours += board.cellStatus(xplus1, yplus1);
    return aliveNeighbours;
}

// Tick to next state use toroidal array to wrap board
void tick(Board& board) {
    for (short x = 0; x < board.xSize(); x++) {
        for (short y = 0; y < board.ySize(); y++) {
            short aliveCount = countAliveNeighbours(board, x, y, board.xSize(), board.ySize());
            if (aliveCount < 2) board.setCell(x, y, false);
            else if (aliveCount == 2 && board.cellStatus(x, y)) board.setCell(x, y, true);
            else if (aliveCount == 3) board.setCell(x, y, true);
            else board.setCell(x, y, false);
        }
    }
}

void randomSeed(Board& board) {
    for (short x = 0; x < board.xSize(); x++) {
        for (short y = 0; y < board.ySize(); y++) {
            board.setCell(x, y, ((double)rand()/RAND_MAX) > 0.5 ? true : false);
        }
    }
}
