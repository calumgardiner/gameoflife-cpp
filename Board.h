//
//  Board.h


#ifndef Board_h
#define Board_h

#include <vector>
#include "Cell.h"

using namespace std;

class Board {
    
private:
    short m_xSize;
    short m_ySize;
    vector< vector<Cell> > cells;
    
public:
    Board(short xSize, short ySize);
    short xSize() const;
    short ySize() const;
    bool cellStatus(short x, short y) const;
    void setCell(short x, short y, bool status);
};

#endif
