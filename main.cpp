//
//  main.cpp

#include <iostream>
#include <vector>
#include "Board.h"
#include "Life.h"
#include "PrintBoard.h"

using namespace std;

int main(int argc, const char * argv[]) {
    if (argc != 4) cout << "usage: " << argv[0] << " [xsize] [ysize] [iterations]" << endl;
    else {
        int x = atoi(argv[1]);
        int y = atoi(argv[2]);
        Board board(x, y);
        randomSeed(board);
        int iterations = atoi(argv[3]);
        for (int i = 0 ; i < iterations; i++) {
            printBoard(board);
            tick(board);
        }
    }
}
