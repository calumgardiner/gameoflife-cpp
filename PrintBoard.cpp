//
//  PrintBoard.cpp


#include <iostream>
#include "PrintBoard.h"
#include <string>

using namespace std;

void clearScreen() {
    for (int n = 0; n < 10; n++)
        printf( "\n\n\n\n\n\n\n\n\n\n" );
}

void printBoard(Board board) {
    clearScreen();
    double xSize = board.xSize();
    double ySize = board.ySize();
    string alive;
    string output;
    for (int x = 0; x < xSize; x++) {
        string row = "";
        for (int y = 0; y < ySize; y++) {
            if (board.cellStatus(x, y)) alive = '#';
            else alive = ' ';
            row += '[';
            row += alive;
            row += ']';
        }
        output += row;
        output += '\n';
    }
    cout << output << endl;
}