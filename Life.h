//
//  Life.h


#ifndef Life__
#define Life__

#include <stdio.h>
#include "Board.h"

void tick(Board& board);

void randomSeed(Board& board);

#endif /* defined(__backup__Life__) */
