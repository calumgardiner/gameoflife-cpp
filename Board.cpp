//
//  Board.cpp


#include "Board.h"

Board::Board(short xSize, short ySize) {
    this->m_xSize = xSize;
    this->m_ySize = ySize;
    // Create a 2D vector with initially alive cells
    vector< vector<Cell> > cells;
    for (short x = 0; x < xSize; x++) {
        vector<Cell> row;
        for(short y = 0; y < ySize; y++) {
            row.push_back(Cell());
        }
        this->cells.push_back(row);
    }
}

short Board::xSize() const {
    return this->m_xSize;
}

short Board::ySize() const {
    return this->m_ySize;
}

bool Board::cellStatus(short x, short y) const {
    return this->cells[x][y].isAlive();
}

void Board::setCell(short x, short y, bool status) {
    if (status) cells[x][y].born();
    else cells[x][y].kill();
}
