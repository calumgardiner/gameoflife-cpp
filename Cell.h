//
//  Cell.h


#ifndef Cell_h
#define Cell_h

class Cell {
    
private:
    bool alive;
    
public:
    Cell();
    Cell(bool alive);
    void kill();
    void born();
    bool isAlive() const;
    
};


#endif
