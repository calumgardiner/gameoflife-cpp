////////////////////////////////////////////////////////////////
//
//  Cell.cpp
//
//  Encapsulates a living or dead cell.
//
////////////////////////////////////////////////////////////////

#include "Cell.h"

Cell::Cell() {
    alive = true;
}

Cell::Cell(bool alive) {
    this->alive = alive;
}

void Cell::kill() {
    this->alive = false;
}

void Cell::born() {
    this->alive = true;
}

bool Cell::isAlive() const {
    return alive;
}