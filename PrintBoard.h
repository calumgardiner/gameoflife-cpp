//
//  PrintBoard.h


#ifndef __backup__PrintBoard__
#define __backup__PrintBoard__

#include <stdio.h>
#include "Board.h"

void printBoard(Board board);

#endif /* defined(__backup__PrintBoard__) */
